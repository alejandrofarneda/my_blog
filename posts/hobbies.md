---
title: "MY HOBBIES"
description:
date: 08 January 2023
---

```
H O B B I E S
```

## SPORTS:

More than a hobby, it is also a daily ground connection. I do sports because it makes me feel good. I have played basketball, practiced martial arts, baseball, but my greatest love is Rugby. Unfortunately, it is a sport that requires perseverance and care that goes beyond the amateur level, which is why I have stopped practicing it.
I also have my bicycle with which I move regardless of the distance.

## PLANTS:

Taking care of my plants it's also one of them, every day I have more and more. I love my plants but I hate choosing pots... that's why I always need my friend's help. One of my biggest dreams is to have a house full of natural light and plants everywhere.

## FOOD & WINE:

For reasons of life itself, I ended up studying sommelier a few years ago and since then I have been able to work with great professionals and taste their food. I have also had the pleasure of representing some well-known wineries in my country. Today I dedicate myself entirely to programming, but eating well, training my sense of smell with each aroma I feel and the good taste for the different torroirs and the wines that come from them will remain one of my hobbies forever.

## ANIMALS:

Especially the dogs. I can't go out without bending down to pet any dog ​​that passes near me. We have a Chihuahua named Simba at home and I am more than sure that there should not be another more beloved dog in the world.

## PHOTOGRAPHY:

These last few years I have bought an old analog Nikon camera and with it I have started a one-way journey. I would like to take a course in analog photography in the future, especially in portraits, which is what I enjoy the most.
In the next posts I will be adding some of my photos... maybe.
