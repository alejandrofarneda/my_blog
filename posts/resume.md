---
title: "DEVELOPER RESUME"
description: Developer resume
date: 16 December 2022
---

```
J U N I O R  F U L L S T A C K  D E V E L O P E R
```

## ABOUT ME:

Fast learner and above all enthusiastic,
proactive and committed to achieving
my goals. I am passionate about
technology and how data analysis has
completely changed our environment.
Half time sportsman, full time
Developer.
I speak Spanish at a bilingual and
advanced English and Italian.

## CONTACT ME:

```
October 8 , 1986 (Nac)
Calle Olta 11 - Valencia - CP. 46026
+ 34 - 622 - 9576 - 13
alejandrofarneda@gmail.com
```

## EXPERIENCE:

### IOT-PRODUCT FRONTEND ENGINEER

```
CECOTEC INNOVACIONES S.L. | August 2021 - Current position | Hybrid
```

```
Development of mobile applications for IOT (React Native)

- React hooks
- Redux y Context
- Graphql, Api rest
- Hexagon architecture
- Design patterns
- Unit testing (Jest)
- TypeScript
- AWS (Amazon web services)
```

### FULL STACK PROJECT

```
HACK A BOSS | February - August 2021 | In Remote
```

```
Developed GAPP - Get Answered Application.

- Redux & Context
- React Route
- NodeJs
- ExpressJs
- MySql
- Complete registration/login with Twilio's SendGrid email
  verification. and Google Login auth service
- User web token (JWT)
```

## SKILLS & TECHNOLOGIES:

### BACK

```
- Node.js & Express
- Graphql & Api Rest
- Insomnia
- MySQL
```

### FRONT

```
- React & React Native
- Redux & Context
- React hooks
- Typescript
```

### OTHER

```
- Git Project management
- Agile methodologies (ClickUp)
- Google Cloud
```

## WORKSHOPS:

```
 - VANILLA JS

 - ADVANCED GIT

 - AGILE METHODOLOGIES

 - WS DEPLOY
```

## TRAINING:

```
2021 | HACK A BOSS La Coruna - Galicia -España

  FULL STACK BOOTCAMP
  - React
  - Node
  - MySql
  - Html
  - Css
  - Javascript
  - More...
```

```
2021 | Un. Nacionál de Córdoba

 - SALES MANAGEMENT

 - TIME MANAGEMENT

 - ENGLISH B2
```

```
2017 | Un. Azafrán - Cordoba

 - PROFESSIONAL SOMMELIER AND BARISTA
```

## LANGUAGES:

```
 ENGLISH - ( B2 + )

 ITALIAN - ( C1 )

 SPANISH - ( NATIVE )
```

```
        VALENCIA - DECEMBER 15 , 2022
```
