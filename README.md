# My Blog

A simple blogging platform builded with [Next.js](https://nextjs.org)

» React

» Next

» Tailwind

» Gray-Matter

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) to see the result.
