import Layout from "../components/Layout";

function About() {
  return (
    <Layout pageTitle="About | My Blog">
      <article className="prose mt-10 mb-10 max-w-none">
        <h2>About me</h2>

        <h3>Hello!</h3>
        <p className="font-medium">
          My name is Alejandro Farneda, I am 36 years old and I am passionate
          not only about programming but also about the teams I am a part of,
          this is the first reason why I love my job so much. In a very short
          time I have had the opportunity to be part of several projects and
          learn from great professionals.
        </p>

        <p className="font-medium">
          In addition to this I have studied and count with more than 10 years
          of experience in commercial management and logistics. Time management
          has always been my great ally.
        </p>

        <p className="font-medium">
          I have created this space to be able to share my capabilities as a
          Junior Frontend in a more simple way. Currently I am actively looking
          for new projects, experiences and I am open to listening to job
          offers.
        </p>
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://drive.google.com/file/d/1x2g6vZcgc_7RyOPfXy9US0eJGLvIjwNv/view?usp=share_link"
        >
          <button className=" mt-10 mb-10 text-white font-bold bg-green-500 rounded-md p-2 hover:bg-green-300">
            Download full resume
          </button>
        </a>

        <p className="font-bold text-lg text-right">Thanks for stopping by!!</p>
      </article>
    </Layout>
  );
}

export default About;
