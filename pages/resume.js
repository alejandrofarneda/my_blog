import Layout from "../components/Layout";

function About() {
  return (
    <Layout pageTitle="Resume | My Blog">
      <iframe
        src="https://drive.google.com/file/d/1x2g6vZcgc_7RyOPfXy9US0eJGLvIjwNv/preview"
        className="border-0"
        width="800"
        height="1200"
      ></iframe>
    </Layout>
  );
}

export default About;
