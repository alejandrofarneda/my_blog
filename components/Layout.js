import Head from "next/head";
import ActiveLink from "./ActiveLink";
import { Icon } from "@iconify/react";
import githubIcon from "@iconify/icons-simple-icons/github";
import linkedinIcon from "@iconify/icons-simple-icons/linkedin";

function Layout({ children, pageTitle }) {
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>{pageTitle}</title>
      </Head>
      <div className="flex flex-col min-h-screen">
        <header className="w-full h-16  flex items-center justify-center ">
          <div className="w-11/12 md:w-full max-w-3xl flex flex-row justify-between ">
            <div className="text-3xl tracking-tighter font-bold text-green-500">
              <ActiveLink href="/">
                <a>H o m e </a>
              </ActiveLink>
            </div>
            <AppNav />
          </div>
        </header>
        <main className="w-11/12 md:w-full max-w-2xl mx-auto my-8 flex-grow">
          {children}
        </main>
        <footer className="flex flex-col items-center justify-center w-full h-24   text-gray-500 ">
          <nav className="flex flex-row">
            <a
              className="mr-6 rounded-xl hover:bg-green-300"
              href="https://gitlab.com/alejandrofarneda"
            >
              <Icon className="w-6 h-6" icon={githubIcon} />
            </a>
            <a
              className="mr-6 rounded-lg hover:bg-green-300"
              href="https://www.linkedin.com/in/alejandro-farneda/"
            >
              <Icon className="w-6 h-6" icon={linkedinIcon} />
            </a>
          </nav>
          <div className="mt-2">Simple Blog - Daniel Alejandro Farneda</div>
        </footer>
      </div>
    </>
  );
}

function AppNav() {
  return (
    <nav className="text-2xl text-gray-400 flex items-center">
      <ActiveLink href="/" activeClassName="text-green-500">
        <a className="mr-8 hover:text-green-500">Blog</a>
      </ActiveLink>
      <ActiveLink href="/about" activeClassName="text-green-500">
        <a className=" mr-8 hover:text-green-500">About</a>
      </ActiveLink>
      <ActiveLink href="/resume" activeClassName="text-green-500">
        <a className="hover:text-green-500">Resume</a>
      </ActiveLink>
    </nav>
  );
}

export default Layout;
